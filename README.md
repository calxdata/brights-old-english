# Brights Old English

Uses [glossing abbreviations](https://en.wikipedia.org/wiki/List_of_glossing_abbreviations) rather than the abbreviations from the text.

## TODO
* add card for memorization of word classes (e.g. Weak noun, Class 3a Strong Verb)
* create scripts to automate creation of an Anki .apkg file to share
    * http://decks.wikia.com/wiki/Anki_APKG_format_documentation
    * https://github.com/kerrickstaley/genanki/blob/master/genanki/__init__.py
